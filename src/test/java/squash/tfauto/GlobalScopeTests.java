package squash.tfauto;

import org.junit.jupiter.api.Test;
import org.opentestfactory.exception.ParameterException;
import org.opentestfactory.exception.ParameterFormatException;
import org.opentestfactory.util.ParameterService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GlobalScopeTests {

    @Test
    public void getGlobalIntTest() throws ParameterException {
        int integer = ParameterService.INSTANCE.getGlobalInt("TC_CUF_junit_testcase");
        assertEquals(50, integer);
    }

    @Test
    public void getGlobalStringTest() throws ParameterException {
        String chars = ParameterService.INSTANCE.getGlobalString("IT_CUF_junit_iteration");
        assertEquals("iterationValue", chars);
    }

    @Test
    public void getGlobalFloatTest() throws ParameterException {
        float floatNumber = ParameterService.INSTANCE.getGlobalFloat("TS_CUF_junit_testsuite");
        assertEquals(62.35f, floatNumber);
    }

    @Test
    public void getGlobalDoubleTest() throws ParameterException {
        double doubleNumber = ParameterService.INSTANCE.getGlobalDouble("CPG_CUF_junit_campaign");
        assertEquals(1.25, doubleNumber);
    }
    
    @Test
    public void getGlobalBooleanTest() throws ParameterException {
        boolean dataset = ParameterService.INSTANCE.getGlobalBoolean("DS_bool");
        assertTrue(dataset, "The dataset param is false");
    }
}
