package squash.tfauto;

import org.junit.jupiter.api.Test;
import org.opentestfactory.exception.ParameterFormatException;
import org.opentestfactory.util.ParameterService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestScopeWithDefaultTests {
    @Test
    public void getTestIntWithDefaultValueTest() throws ParameterFormatException {
        int integer = ParameterService.INSTANCE.getTestInt("TC_CUF_junit_testcase", 50);
        assertEquals(50, integer);
    }

    @Test
    public void getTestStringWithDefaultValueTest() {
        String chars = ParameterService.INSTANCE.getTestString("IT_CUF_junit_iteration", "iterationValue");
        assertEquals("iterationValue", chars);
    }

    @Test
    public void getTestFloatWithDefaultValueTest() throws ParameterFormatException {
        float floatNumber = ParameterService.INSTANCE.getTestFloat("TS_CUF_junit_testsuite", 62.35f);
        assertEquals(62.35f, floatNumber);
    }

    @Test
    public void getTestDoubleWithDefaultValueTest() throws ParameterFormatException {
        double doubleNumber = ParameterService.INSTANCE.getTestDouble("CPG_CUF_junit_campaign", 1.25);
        assertEquals(1.25, doubleNumber);
    }

    @Test
    public void getTestBooleanWithDefaultValueTest() {
        boolean dataset = ParameterService.INSTANCE.getTestBoolean("DS_bool", true);
        assertTrue(dataset, "The dataset param is false");
    }
}
