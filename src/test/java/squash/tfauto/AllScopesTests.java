package squash.tfauto;

import org.junit.jupiter.api.Test;
import org.opentestfactory.exception.ParameterException;
import org.opentestfactory.exception.ParameterFormatException;
import org.opentestfactory.util.ParameterService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AllScopesTests {

    @Test
    public void getIntTest() throws ParameterException {
        int integer = ParameterService.INSTANCE.getInt("TC_CUF_junit_testcase");
        assertEquals(50, integer);
    }

    @Test
    public void getStringTest() throws ParameterException {
        String chars = ParameterService.INSTANCE.getString("IT_CUF_junit_iteration");
        assertEquals("iterationValue", chars);
    }

    @Test
    public void getFloatTest() throws ParameterException {
        float floatNumber = ParameterService.INSTANCE.getFloat("TS_CUF_junit_testsuite");
        assertEquals(62.35f, floatNumber);
    }

    @Test
    public void getDoubleTest() throws ParameterException {
        double doubleNumber = ParameterService.INSTANCE.getDouble("CPG_CUF_junit_campaign");
        assertEquals(1.25, doubleNumber);
    }

    @Test
    public void getBooleanTest() throws ParameterException {
        boolean dataset = ParameterService.INSTANCE.getBoolean("DS_bool");
        assertTrue(dataset, "The dataset param is false");
    }
}
