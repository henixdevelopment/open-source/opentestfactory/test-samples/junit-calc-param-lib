package squash.tfauto;

import org.junit.jupiter.api.Test;
import org.opentestfactory.exception.ParameterFormatException;
import org.opentestfactory.util.ParameterService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AllScopesWithDefaultTests {
    @Test
    public void getIntWithDefaultValueTest() throws ParameterFormatException {
        int integer = ParameterService.INSTANCE.getInt("TC_CUF_junit_testcase", 50);
        assertEquals(50, integer);
    }

    @Test
    public void getStringWithDefaultValueTest() {
        String chars = ParameterService.INSTANCE.getString("IT_CUF_junit_iteration", "iterationValue");
        assertEquals("iterationValue", chars);
    }

    @Test
    public void getFloatWithDefaultValueTest() throws ParameterFormatException {
        float floatNumber = ParameterService.INSTANCE.getFloat("TS_CUF_junit_testsuite", 62.35f);
        assertEquals(62.35f, floatNumber);
    }

    @Test
    public void getDoubleWithDefaultValueTest() throws ParameterFormatException {
        double doubleNumber = ParameterService.INSTANCE.getDouble("CPG_CUF_junit_campaign", 1.25);
        assertEquals(1.25, doubleNumber);
    }

    @Test
    public void getBooleanWithDefaultValueTest() {
        boolean dataset = ParameterService.INSTANCE.getBoolean("DS_bool", true);
        assertTrue(dataset, "The dataset param is false");
    }
}
