package squash.tfauto;

import org.junit.jupiter.api.Test;
import org.opentestfactory.exception.ParameterException;
import org.opentestfactory.exception.ParameterFormatException;
import org.opentestfactory.util.ParameterService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GlobalScopeWithDefaultTests {
    @Test
    public void getGlobalIntWithDefaultValueTest() throws ParameterException {
        int integer = ParameterService.INSTANCE.getGlobalInt("TC_CUF_junit_testcase", 50);
        assertEquals(50, integer);
    }

    @Test
    public void getGlobalStringWithDefaultValueTest() throws ParameterException {
        String chars = ParameterService.INSTANCE.getGlobalString("IT_CUF_junit_iteration", "iterationValue");
        assertEquals("iterationValue", chars);
    }

    @Test
    public void getGlobalFloatWithDefaultValueTest() throws ParameterException {
        float floatNumber = ParameterService.INSTANCE.getGlobalFloat("TS_CUF_junit_testsuite", 62.35f);
        assertEquals(62.35f, floatNumber);
    }

    @Test
    public void getGlobalDoubleWithDefaultValueTest() throws ParameterException {
        double doubleNumber = ParameterService.INSTANCE.getGlobalDouble("CPG_CUF_junit_campaign", 1.25);
        assertEquals(1.25, doubleNumber);
    }

    @Test
    public void getGlobalBooleanWithDefaultValueTest() throws ParameterException {
        boolean dataset = ParameterService.INSTANCE.getGlobalBoolean("DS_bool", true);
        assertTrue(dataset, "The dataset param is false");
    }
}
