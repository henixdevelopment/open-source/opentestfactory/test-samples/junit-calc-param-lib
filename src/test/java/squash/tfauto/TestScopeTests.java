package squash.tfauto;

import org.junit.jupiter.api.Test;
import org.opentestfactory.exception.ParameterException;
import org.opentestfactory.util.ParameterService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class TestScopeTests {

    @Test
    public void getTestIntTest() throws ParameterException {
        int integer = ParameterService.INSTANCE.getTestInt("TC_CUF_junit_testcase");
        assertEquals(50, integer);
    }
    
    @Test
    public void getTestStringTest() throws ParameterException {
        String chars = ParameterService.INSTANCE.getTestString("IT_CUF_junit_iteration");
        assertEquals("iterationValue", chars);
    }
    
    @Test
    public void getTestFloatTest() throws ParameterException {
        float floatNumber = ParameterService.INSTANCE.getTestFloat("TS_CUF_junit_testsuite");
        assertEquals(62.35f, floatNumber);
    }

    @Test
    public void getTestDoubleTest() throws ParameterException {
        double doubleNumber = ParameterService.INSTANCE.getTestDouble("CPG_CUF_junit_campaign");
        assertEquals(1.25, doubleNumber);
    }
    
    @Test
    public void getTestBooleanTest() throws ParameterException {
        boolean dataset = ParameterService.INSTANCE.getTestBoolean("DS_bool");
        assertTrue(dataset, "The dataset param is false");
    }
    
}
