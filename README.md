This project is used to test parameter injection with Squash AUTOM/DEVOPS in a JUnit project.

The tests are organized based on 2 criteria, the scope and the presence of a default value.

The available classes are :

* *GlobalScopeTests* contains all the tests using the global scope
* *GlobalScopeWithDefaultTests* contains all the tests using the global scope with a default value already set
* *TestScopeTests* contains all the tests using the test scope
* *TestScopeWithDefaultTests* contains all the tests using the test scope with a default value already set
* *AllScopeTests* contains all the tests checking both scopes
* *AllScopeWithDefaultTests* contains all the tests checking both scopes with a default value already set

Expected parameter list for a successful test :

* TC_CUF_junit_testcase : 50

* IT_CUF_junit_iteration : iterationValue

* TS_CUF_junit_testsuite : 62.35f

* CPG_CUF_junit_campaign : 1.25

* DS_bool : true

Possible exceptions :

* ParameterNotFoundException : Any non-default method with the parameter not set

* ParameterFormatException :  Any method with a parameter from a different type as the expected one
